package com.esalnikov.downloadUtility;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class DownloadUtility {
    
    /**
     * Default value of number of threads if it is not set in command line
     */
    private static int numOfThreads = 3;
    
    /**
     * Default value of speed limit
     */
    private static int speedLimit = 10240;
    
    /**
     * Default value of path of file with links
     */
    private static String pathToFileWithLinks = "/var/www/test_links.txt";
    
    /**
     * Default value of path to downloaded files
     */
    private static String folderToDownloadedFiles = "/var/www/downloaded_files";
    
    /**
     * Total downloaded bytes
     */
    private static volatile long totalBytesDownloaded = 0;
    
    /**
     * Increases total downloaded bytes
     * @param increment 
     */
    public static synchronized void incTotalBytesDownloaded(int increment) {
        totalBytesDownloaded += increment;
    }

    /**
     * Reads lines from text file
     * @return
     * @throws FileNotFoundException
     * @throws IOException
     */
    private static List<String[]> getLinesFromTextFile() {
        List<String[]> linksList = new ArrayList<>();
        try (BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream(pathToFileWithLinks), StandardCharsets.UTF_8
                )
        )) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                String[] lineArray;
                String delimiter = " ";
                lineArray = line.split(delimiter);
                linksList.add(lineArray);
            }
        } catch (IOException e) {
            //
        }
        return linksList;
    }
    
    /**
     * Initiates input arguments
     * @param args 
     */
    private static void initArgs(String[] args) {
        for (int i = 0; i < args.length; i++) {
            if (args[i].equals("-n")) {
                numOfThreads = Integer.parseInt(args[i + 1]);
            } else if (args[i].equals("-l")) {
                String strSpeed = args[i + 1];
                if (strSpeed.endsWith("k")) {
                    speedLimit = Integer.parseInt(strSpeed.substring(0, strSpeed.length() - 1)) * 1024;
                } else if (strSpeed.endsWith("m")) {
                    speedLimit = Integer.parseInt(strSpeed.substring(0, strSpeed.length() - 1)) * 1024 * 1024;
                } else {
                    speedLimit = Integer.parseInt(strSpeed);
                }
            } else if(args[i].equals("-f")) {
                pathToFileWithLinks = args[i+ 1];
            } else if (args[i].equals("-o")) {
                folderToDownloadedFiles = args[i+ 1];
            }
        }
    }    

    /**
     * Main method
     * @param args
     */
    public static void main(String[] args) {

        initArgs(args);
        
        final List<String[]> list = getLinesFromTextFile();
        
        final ExecutorService executorService = Executors.newFixedThreadPool(numOfThreads);
        
        final long start = System.currentTimeMillis();
               
        for (String[] str : list) {
            executorService.execute(new Downloader(str[0], folderToDownloadedFiles + "/" + str[1], speedLimit));
        }
        
        executorService.shutdown();
        
        try {
            executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
        }
        
        final long end = System.currentTimeMillis();
               
        System.out.println("Downloading is successfully done!");
        System.out.println("Total downloaded: " + totalBytesDownloaded + " bytes.");
        System.out.println("Total time: " + (end - start) + " ms.");
        
    }
}
