package com.esalnikov.downloadUtility;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class Downloader implements Runnable {
    
    private final String strUrl;
    
    private final String filePath;
    
    private final int speedLimit;
    
    private final int BUFFER_SIZE = 16384;
    
    public Downloader(String strUrl, String filePath, int speedLimit) {
        this.strUrl = strUrl;
        this.filePath = filePath;
        this.speedLimit = speedLimit;
    }

    @Override
    public void run() {        
        URL url = null;
        BufferedInputStream bufferedInputStream = null;
        FileOutputStream fileOutputStream = null;        
        try {
            url = new URL(this.strUrl);
            bufferedInputStream = new BufferedInputStream(url.openStream());
            fileOutputStream = new FileOutputStream(this.filePath);
            byte[] buffer = new byte[BUFFER_SIZE];
            int count;            
            while (true) {
                long start = System.currentTimeMillis();
                count = bufferedInputStream.read(buffer, 0, BUFFER_SIZE);                
                int activeThreadsCount = Thread.activeCount() - 1;
                long timeout = (long) Math.floor(BUFFER_SIZE * 1000000L * activeThreadsCount / this.speedLimit);
                long end = System.currentTimeMillis();
                long readTime = (end - start) * 1000;
                long delay = (timeout - readTime < 0) ? 0 : timeout - readTime;
                if (count != -1) {
                    fileOutputStream.write(buffer, 0, count);
                    DownloadUtility.incTotalBytesDownloaded(count);
                    TimeUnit.MICROSECONDS.sleep(delay);
                } else {
                    break;
                }
            }
        } catch (InterruptedException e) {
            System.out.println("Interrupted");
        } catch (MalformedURLException e) {
            System.out.println("URL " + url + " is incorrect:" + e.getMessage());
        } catch (IOException e) {
            System.out.println("Could not open URL " + url + ": " + e.getMessage());
        }
        
        if (fileOutputStream != null) {
            try {
                fileOutputStream.close();
            } catch (IOException e) {
            }
        }
        
        if (bufferedInputStream != null) {
            try {
                bufferedInputStream.close();
            } catch (IOException e) {
            }
        }        
    }
}
